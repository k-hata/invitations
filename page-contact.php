<?php

/*
Template Name: お問い合わせページ
*/
?>

<?php get_header(); ?>
<?php echo do_shortcode('[metaslider id="44"]'); ?>
<div class="pdb50 container-fluid">
<div class="logoMarkSub">
	<img src="<?php echo get_template_directory_uri(); ?>/images/contactLogoMark.png" alt="お問い合わせロゴ">
</div>


<div class="container-fluid">
<div class="container">
	<div class="row">
		<div class="pd30 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fadein">
		<p class="contentsTitle em250 skyBlue">CONTACT</p>
		<p class="contentsSubTitle center width150 em200 contentsSubTitleBarB">お問い合わせ</p>
		</div>
	</div>
</div>
</div>


<div class="container">
	<div class="row">
		<div class="pdb100 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fadein">
		<p class="mgb30 center lh15 em150 bold em250 skyBlue">お電話でお問い合わせはこちらから<br>お気軽にご連絡ください<br>相談・調査・見積無料</p>
		</div>
	</div>
</div>
</div>


<div class="pd30200 container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fadein">
		<div class="afterFollow pd30">
		<h3 class="mg0150 center skyBlue em200 black italic"><span class="fa-stack  tell-mark"><i class="fas fa-circle fa-stack-2x blue-color"></i><i class="fas fa-phone fa-stack-1x fa-flip-horizontal text-white"></i></span>0776-23-3537</h3>

		<p class="center lh15bold">受付時間：9:00-17:00<br>日曜、夜間の場合は090-1630-2429の方へご連絡下さい<br>（土日祝・夏季休暇・年末年始・会社休業日は除く）</p>
		</div>
		</div>
	</div>
</div>


<div class="pd1000 container">
	<div class="row">
		<div class="mgb100 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 fadein">
		<p class="em100" >この度は、株式会社第一環研の害虫・害獣駆除（シロアリ、ゴキブリ、ハチ、ハチの巣、ねずみ、ダニ、毛虫、チョウバエ、ハクビシン、アライグマ、イタチ）のホームページにアクセスいただき、誠にありがとうございます。当社では、福井県を中心に害虫駆除による環境衛生のコンサルティングを行っております。責任をもって対応にあたり、施工後のメンテナンスや急なトラブルにも迅速に対処します。害虫・害獣（シロアリ、ゴキブリ、ハチ、ハチの巣、ねずみ、ダニ、毛虫、チョウバエ、ハクビシン、アライグマ、イタチ）被害にお困りでしたら、お気軽にご相談ください。</p>
		</div>
	</div>
</div>


<?php echo do_shortcode('[contact-form-7 id="48" title="お問い合わせ"]'); ?>


<section class="pd1000 gaityu_sec">
<div class="container">
<div class="row">
<div class="danraku">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">シロアリの防除処理（シロアリ特約保険付保証）</div>
シロアリ防除は新築時に行う「予防」と既存建築物に行う「駆除」があります。<br />
・土壌処理（土からの侵入を防除します）<br />
・木部処理（地面から１ｍまでの高さの水回りを中心に防除します）<br />
・ベイト工法（毒餌を用いて防除します）<br />
・防腐処理（木材につく腐朽菌を発生させないようにします）<br />
</div>
    
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">ネズミの防除・防除処理</div>
・ネズミの巣の材料を無くす、通路の遮断、直接の駆除を行います。<br />
</div>
    
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">ネズミの防除・防除処理</div>
・害虫の生息数の調査、発生源の清掃、液剤散布・燻煙処理などを行います。<br />
</div>
    
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">害虫防除</div>
・害虫の生息数の調査、発生源の清掃、液剤散布・燻煙処理などを行います。<br />
</div>
    
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">害獣</div>
・害獣捕獲に必要な届け出を申請し、捕獲、通路の遮断、消毒、破損部分の修繕を行います。<br />
・小動物の捕獲（アライグマ・ハクビシン・イタチ　等）<br />
</div>
    
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="font8">その他</div>
・建築物衛生法で年２回3000平米以上の建築に義務付けられるネズミ、昆虫等の防除の調査・施工を行います。<br />
・床下強制換気システム工事を行います。
</div>
</div>
</div>
</div>
</section>


<div class="pd500 container-fluid">
</div>


<?php get_footer(); ?>
