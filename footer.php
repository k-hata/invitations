<footer class="Footer">

<canvas id="cvs" class="cvsfront" width="1600px" height="1150px">
</canvas>
    <div class="container mindiv">
		<div class="BkWhite p-5">
        <div class="row text-white">

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 pt-3 pb-2">
				<p><h3 class="lborder"><nobr>サービス事業</nobr></h2></p>
				<ul>
					<li class="footer pt-3"><a href="#" class="white">Blooming kids こはる<br />（放課後デイサービス）</a></li>
					<li class="footer pt-3"><a href="#" class="white">Koharu Walfare Enegy<br />（ガソリンスタンド）</a></li>
					<li class="footer pt-3"><a href="#" class="white">地域の便利屋&nbsp;KOHARU<br />（便利屋）</a></li>
					<li class="footer pt-3"><a href="#" class="white">サポートステーション心桜<br />（就労継続支援&nbsp;B型事業所）</a></li>
					<li class="footer pt-3"><a href="#" class="white">ライフサポート心桜<br />（障害者向けアパート）</a></li>
					<li class="footer pt-3"><a href="#" class="white">相談支援事業所<br />（相談支援）</a></li>
				</ul>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 pt-3 pb-2">
				<p><h3 class="lborder"><a href="#" class="white">利用者募集</a></h2></p>
				<br />
				<p><h3 class="lborder"><a href="#" class="white">会社情報</a></h2></p>
				<br />
				<p><h3 class="lborder"><nobr>お問い合わせ</nobr></h2></p>
				<ul>
					<li class="footer pt-3"><a href="#" class="white">利用者募集</a></li>
					<li class="footer pt-3"><a href="#" class="white">お仕事募集</a></li>
					<li class="footer pt-3"><a href="#" class="white">会社概要</a></li>
					<li class="footer pt-3"><a href="#" class="white">お問い合わせ</a></li>
					<li class="footer pt-3"><a href="#" class="white">アクセス</a></li>
					<li class="footer pt-3"><a href="#" class="white">プライバシーポリシー</a></li>
				</ul>
			</div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 pt-3">

            <div class="lh15 pt-3">
            〒325-0027<br />
            栃木県那須塩原市共墾社149-2<br />
            <i class="fas fa-phone-square text-pink"></i>0287-73-8866&nbsp;<br class="d-xl-none d-lg-none d-md-none d-sm-inline">
            <i class="fas fa-fax text-pink"></i>0287-73-8867<br />
            </div>

            </div>
        </div>
		</div>
    </div>

</footer>
<div class="">Copyright&nbsp;©&nbsp; All rights reserved.</div>

<!-- Bootstrap4 jQuery Popper CDN -->
<script
  src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- sticky navbar -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sticky.js"></script>

<!-- match height -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>

<!-- <script>
$(function($){
	$('.').matchHeight();
});
</script> -->

<!-- Yamm3 -->
<script>
jQuery(document).on('click', '.yamm .dropdown-menu', function(e) {
   e.stopPropagation()
})
</script>

<script>
$(function($){
    $(window).scroll(function (){
        $('.fadein').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 200){
                $(this).addClass('scrollin');
            }
        });
    });
});
</script>

<script>
$(function($){
  function initFontSize() {
    var size = (sessionStorage.getItem('fontSize'))? sessionStorage.getItem('fontSize') : '16';
    changeFontSize(size);
  }
  
  function changeFontSize(size){
    $('html').css('font-size', size + 'px');
    $('[data-font!=' + size + ']').removeClass('active');
    $('[data-font=' + size + ']').addClass('active');
    sessionStorage.setItem('fontSize', size);
  }
  
  function addListeners() {
    $('[data-font]').on('click', function(){
      changeFontSize($(this).data('font'));
    });
  }
  
  function init() {
    initFontSize();
    addListeners();
  }
  
  init();
});
</script>

<!-- 桜吹雪 -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sakura.8222.7.js"></script>

<?php wp_footer(); ?>
</body>
</html>