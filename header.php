<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0,initial-scale=1.0" />
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>

<!-- Bootstrap4 -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<!-- CSS -->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
<!-- Responsive CSS -->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/responsive.css" rel="stylesheet">
<!-- Noto Sans Web font -->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
<!-- Yamm3 -->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/yamm.css" rel="stylesheet">
<!-- print CSS -->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/print.css" rel="stylesheet" media="print">
	

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<meta name="google-site-verification" content="pfS2MJ3FKZwjaewn5tDTRd3EeSgwWEDA033TYvl6a70" />
</head>
<body <?php body_class (); ?>>
<?php wp_head(); ?>

    
    <nav class="navbar yamm navbar-expand-md navbar-light bg-white sticky-top">
        <a class="navbar-brand" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/koharu_logo.jpg" width="200px"></a>
        <!-- Toggle Button -->
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#headerCollapse" aria-controls="navbarNav" aria-expanded="false" aria-label="#headerCollapse">
			<i class="fa fa-bars fa-lg"></i>
			</button>
        
        <div class="container">
        <div class="collapse navbar-collapse" id="headerCollapse">
            <ul class="navbar-nav w-100 nav-justified">
                <li class="nav-item">
                    <a class="nav-link" href="#">ホーム<br><span class="nav-subtitle">Home</span></a>
                </li>
                <li class="nav-item dropdown yamm-fw">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">サービス事業<br><span class="nav-subtitle">Service</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="triangle-1 d-none d-md-inline-block"></div>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 py-1 d-none d-md-block text-white"><p class="text-center dropdown-subtitle">Service</p><h4 class="text-center pt-1 dropdown-title">サービス事業</h4></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-child fa-3x mb-2 text-red circle-child d-none d-md-inline-block"></i><br class="d-none d-md-block">Blooming&nbsp;kids&nbsp;こはる<br>（放課後デイサービス）</a></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-gas-pump fa-3x mb-2 text-red circle-gas d-none d-md-inline-block"></i><br>KOHARU&nbsp;Welfare&nbsp;Energy<br>（ガソリンスタンド）</a></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-store-alt fa-3x mb-2 text-red circle-store d-none d-md-inline-block"></i><br>地域の便利屋&nbsp;KOHARU<br>（便利屋）</a></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="far fa-handshake fa-3x mb-2 text-red circle-hands d-none d-md-inline-block"></i><br>サポートステーション心桜<br>（就労継続支援&nbsp;B型事業所）</a></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-hand-holding-heart fa-3x mb-2 text-red circle-heart d-none d-md-inline-block"></i><br>ライフサポート心桜<br>（障害者向けアパート）</a></div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-comments fa-3x mb-2 text-red circle-comments d-none d-md-inline-block"></i><br>相談支援事業所<br>（相談支援）</a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">利用者募集<br><span class="nav-subtitle">Recruit</span></a>
                </li>
                <li class="nav-item dropdown yamm-fw">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">会社情報<br><span class="nav-subtitle">Company</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="triangle-2 d-none d-md-inline-block"></div>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 py-1 d-none d-md-block text-white"><p class="text-center dropdown-subtitle">Company</p><h4 class="text-center pt-1 dropdown-title">会社情報</h4></div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="far fa-building fa-3x mb-2 text-red circle-buil d-none d-md-inline-block"></i><br class="d-none d-md-block">会社概要</a></div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-map-marker-alt fa-3x mb-2 text-red circle-mark d-none d-md-inline-block"></i><br>アクセス</a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown yamm-fw">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">お問い合わせ<br><span class="nav-subtitle">Contact</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="triangle-3 d-none d-md-inline-block"></div>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 py-1 d-none d-md-block text-white"><p class="text-center dropdown-subtitle">Contact</p><h4 class="text-center pt-1 dropdown-title">お問い合わせ</h4></div>
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-briefcase fa-3x mb-2 text-red circle-briefcase d-none d-md-inline-block"></i><br class="d-none d-md-block">お仕事募集</a></div>
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-phone fa-3x mb-2 text-red circle-phone d-none d-md-inline-block"></i><br>お問い合わせ</a></div>
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 text-center py-1"><a class="dropdown-item" href="#"><i class="fas fa-user-shield fa-3x mb-2 text-red circle-shield d-none d-md-inline-block"></i><br>プライバシーポリシー</a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <a class="d-none d-md-inline-block"><button class="size-button active" data-font="16">中</button></a>
                <a class="d-none d-md-inline-block"><button class="size-button" data-font="20">大</button></a>
            </ul>
        </div>
        </div>
    </nav>