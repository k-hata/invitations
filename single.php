<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12 mb-5">
            <div class="row">
                <?php if(have_posts()): the_post(); ?>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4 class=""><?php echo get_the_title(); ?></h4>
                    <h5 class=""><?php the_time('Y.m.d'); ?></h5>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class=""><?php the_post_thumbnail('large'); ?></div>
                    <div class=""><?php the_content(); ?></div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 d-flex align-items-end">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>